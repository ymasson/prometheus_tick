---
title: Prometheus / TICK
subtitle:
description:
author: Yann Masson
theme: moon
separator: <!--s-->
revealOptions:
  width: 1280
  height: 1024
  center: false
  transition: 'fade'
---

## Prometheus / TICK
Présentation rapide

<!--s-->
## L'héritage
Depuis longtemps la supervision est un point clé de tout système.
Certains outils ont plus de 20ans.
Nagios par exemple est né en 1996.
L'évolution des infrastructures a introduit plus de souplesse et d'élasticité.
De nouveaux outils sont donc nés.

<!--s-->
## Vocabulaire
* Métrologie (Metrics): collecte de données permettant de détecter des changements et de comprendre l'état courant du système et son historique.
* Supervision (Monitoring): serveillance de l'état du système et identification des événements intéressants.
* Alerte (Alerting): notification de la bonne personne dont l'intervention est requise.

<!--s-->
## Les protagonistes
||||
|---|---|---|
| | Prometheus | TICK |
| <small>Proprietaire</small> | <small>Linux Foundation</small> | <small>InfluxData</small> |
| <small>Création</small> | <small>Nov. 2012 (V1.0 Jul. 2016)</small> | <small>v1.0 Sept. 2016</small> |
| <small>Licence</small> | <small>Apache License 2.0</small> | <small>MIT</small> |
| <small>Collecte</small> | <small>Pull, Push</small> | <small>Push, Pull</small> |
| <small>Stockage</small> | <small>oui</small> | <small>oui</small> |
| <small>Plugins</small> | <small>non</small> | <small>oui</small> |

<!--s-->
## Prometheus
![prometheus_architecture](images/prometheus_architecture.png)<!-- .element width="100%" -->

<!--s-->
## Prometheus server
* démon central
* collecte les metrics à partir d'une liste de targets, en pull uniquement
* stocke les metrics dans une TSDB locale
* permet de définir des règles d'alerte
* permet la transformation et le relabel des metrics
* permet une visualisation simple des metrics

<!--s-->
## Prometheus
![prometheus_architecture](images/prometheus_architecture.png)<!-- .element width="100%" -->

<!--s-->
## Prometheus Exporter
* agents exposant les metrics
* chaque exporter écoute sur un port dédié
* node_exporter pour les metrics système GNU Linux (cpu, ram, disk,..)
* blackbox_exporter pour les metrics sur des tests réseaux (ping, http,..)
* pushgateway , permet de travailler en mode push et d'injecter des metrics externes
* wmi_exporter pour les metrics système et middleware Windows
* haproxy_exporter pour les metrics HAproxy sur GNU Linux
* mysqld_exporter pour les metrics MySQL sur GNU Linux
* ...

<!--s-->
## Prometheus
![prometheus_architecture](images/prometheus_architecture.png)<!-- .element width="100%" -->

<!--s-->
## Prometheus Alertmanager
* démon prenant en charge les alertes et l'aiguillage de celle ci
* peut envoyer les alertes vers différents systèmes (Slack, PagerDuty, mail,..)
* permet de gérer une chaine d'alerting en notifiant différent niveaux d'intervention

<!--s-->
## TICK
Telegraf / InfluxDB / Chronograf / Kapacitor
![tick_architecture](images/tick_architecture.png)<!-- .element width="100%" -->

<!--s-->
## Telegraf
* agent "multifonctions" basé sur des 'plugins'
* collecte les metrics par des 'input plugins'
* push les metrics par des 'output plugins' et offre un choix de différent format (InfluxDB, Graphite, Elasticsearch,...)
* capable d'integrer des metrics externes en pull
* capable de transformer les metrics avant des les envoyer (aggregator and processor plugins)

<!--s-->
## TICK
![tick_architecture](images/tick_architecture.png)<!-- .element width="100%" -->

<!--s-->
## InfluxDB
* TSDB = Time Series Data Base
* stocke les metrics

<!--s-->
## TICK
![tick_architecture](images/tick_architecture.png)<!-- .element width="100%" -->

<!--s-->
## Kapacitor
* Permet un traitement sur les metrics
* Permet travailler sur les metrics en temps réel (stream) ou sur un échantillon de temps (batch)
* Peux transformer des metrics venant de la TSDB et les push vers la TSDB
* Permet de définir des règles d'alerte et de les aiguiller vers la bonne personne
* Peut envoyer les alertes vers différents systèmes via des 'plugins' (Slack, PagerDuty, mail,..)

<!--s-->
## TICK
![tick_architecture](images/tick_architecture.png)<!-- .element width="100%" -->

<!--s-->
## Chronograf
* dashboard
* visualisation des metrics
* visualisation des alertes et de leur historique
* Peut être remplacé par Grafana

<!--s-->
## Résumons
* Prometheus server est le point central et concentre une grosse partie du traitement. Collecte, Stockage, Règles.
* Les Exporter sont des satellites qui ne se chargent que de l'exposition des metrics, et necessitent autant de flux réseaux que leur nombre
* Le point de concentration de la stack TICK est InfluxDB qui stocke les metrics
* Telegraf et Kapacitor sont des briques indépendentes avec chacune un rôle.
* Telegraf se charge du traitement des metrics (collecte, transformation) et les envoi à la TSDB. Il équivaut à tous les Prometheus Exporter et une partie du Prometheus Server.
* Kapacitor se charge de l'analyse et des alertes. Il équivaut à une partie du Prometheus Server et à l'Alertmanager.
* Alertmanager peut gerer la chaine d'alerting et les escalades, la où il faudra un système externe à la stack TICK.
* Les flux réseaux de la stack TICK sont limités
* Prometheus à besoin de la liste des targets la ou chaque agent Telegraf va envoyer ses metrics vers le stockage
